## Project description / User journey

In this simple twitter clone project, a user can make a new registration and login into the system. 

**Landing page** is where tweet can be created with an image attached. Real-time validation and image preview is performed using Livewire dynamically.
After creating the tweet, it's shown into the timeline where can be liked/disliked or Removed. This also is the place where all tweets are shown
for all followed users.

**Explore page** is where all registered users can be found. Their profile page is the place where they
can be followed/unfollowed. Also, description and timeline of their tweets are presented. 

**Notifications page** shows who started to follow you and it's updating dynamically/real-time using Livewire. New and old notifications functionality is available.

**Personal profile page** is where user description and tweets are. The timeline is the same as on the landing page except it doesn't show users tweets that have been followed.
In **Edit profile** the user can add a new description, avatar, banner, or even change its password.