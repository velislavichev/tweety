<?php

namespace App\Http\Controllers;

use App\User;

class ExploreController extends Controller
{
    /**
     * Return explore view
     *
     * @return view
     */
    public function __invoke()
    {
        return view('explore', [
            'users' => User::paginate(50)
        ]);
    }
}
