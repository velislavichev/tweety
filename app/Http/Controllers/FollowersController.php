<?php

namespace App\Http\Controllers;

use App\Notifications\Followed;
use App\User;
use Illuminate\Support\Facades\Notification;

class FollowersController extends Controller
{
    /**
     * Follow new person 
     *
     * @param User $user
     * @return redirect
     */
    public function store(User $user)
    {
        $currentUser = auth()->user();

        $currentUser->toggleFollow($user);

        $user->notify(new Followed($currentUser->username));

        if (!$currentUser->following($user)) {
            return redirect()->back()->with('success', __("Oh no! You unfollowed $user->username!"));
        }

        return redirect()->back()->with('success', __("Yes! You followed $user->username!"));
    }
}
