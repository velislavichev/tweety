<?php

namespace App\Http\Controllers;

class NotificationsController extends Controller
{
    /**
     * Return notifications view
     *
     * @return view
     */
    public function __invoke()
    {
        return view('notifications.show', [
            'unreadNotifications' => tap(auth()->user()->unreadNotifications)->markAsRead(),
            'readNotifications' => auth()->user()->notifications,
        ]);
    }
}
