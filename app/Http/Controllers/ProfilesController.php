<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProfilesController extends Controller
{
    /**
     * Explore profiles page 
     *
     * @param User $user
     * @return view
     */
    public function show(User $user)
    {
        return view('profiles.show', [
            'user' => $user,
            'tweets' => $user->tweets()
                ->withLikes()
                ->paginate(50)
        ]);
    }

    /**
     * Edit profile page
     *
     * @param User $user
     * @return view
     */
    public function edit(User $user)
    {
        return view('profiles.edit', compact('user'));
    }

    /**
     * Update profile method
     *
     * @param User $user
     * @return redirect
     */
    public function update(User $user)
    {
        $attributes = request()->validate([
            'username' => ['string', 'required', 'max:255', Rule::unique('users')->ignore($user)],
            'name' => ['string', 'required', 'max:255'],
            'description' => ['nullable', 'string', 'max:400'],
            'avatar' => ['nullable', 'file', 'image'],
            'banner' => ['nullable', 'file', 'image'],
            'email' => ['string', 'required', 'email', 'max:255', Rule::unique('users')->ignore($user)],
            'password' => ['nullable', 'string', 'min:8', 'max:255', 'confirmed'],
        ]);

        if (request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        if (request('banner')) {
            $attributes['banner'] = request('banner')->store('banner');
        }

        $user->update($attributes);

        return redirect($user->path())->with('success', __('Profile data changed!'));
    }
}
