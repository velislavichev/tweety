<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Http\Request;

class TweetsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('tweets.index', [
            'tweets' => auth()->user()->timeline()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store()
    {
        $attributes = request()->validate([
            'body' => 'required|max:255',
            'image' => 'file',
        ]);

        if (request('image')) {
            $attributes['image'] = request('image')->store('images');
        }

        Tweet::create([
            'user_id' => auth()->id(),
            'body'    => $attributes['body'],
            'image'   => $attributes['image'] ?? null,
        ]);

        return redirect()->route('home')->with('success', __('Yay! New tweet!'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tweet $tweet
     * @return redirect
     */
    public function destroy(Tweet $tweet)
    {
        Tweet::destroy($tweet->id);

        return back()->with('success', __('Tweet removed!'));
    }

    /**
     * Like method
     *
     * @param Tweet $tweet
     * @return redirect
     */
    public function like(Tweet $tweet)
    {
        $tweet->like();

        return redirect()->back();
    }

    /**
     * Dislike method
     *
     * @param Tweet $tweet
     * @return redirect
     */
    public function dislike(Tweet $tweet)
    {
        $tweet->dislike();

        return redirect()->back();
    }
}
