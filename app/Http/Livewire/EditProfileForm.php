<?php

namespace App\Http\Livewire;

use App\User;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditProfileForm extends Component
{
    // use WithFileUploads;

    /**
     * User attributes
     *
     * @var string
     */
    public $user, $username, $name, $avatar, $banner, $description, $email, $password, $password_confirmation;

    public function mount(User $user)
    {
        $this->user = $user;
        $this->username = $user->username;
        $this->name = $user->name;
        $this->description = $user->description;
        $this->email = $user->email;
    }

    /**
     * Store validation rules
     *
     * @var array
     */
    public function rules()
    {
        return [
            'username' => ['string', 'required', 'max:255', Rule::unique('users')->ignore($this->user)],
            'name' => ['string', 'required', 'max:255'],
            'description' => ['nullable', 'string', 'max:400'],
            'avatar' => ['nullable', 'file', 'image'],
            'banner' => ['nullable', 'file', 'image'],
            'email' => ['string', 'required', 'email', 'max:255', Rule::unique('users')->ignore($this->user)],
            'password' => ['nullable', 'string', 'min:8', 'max:255', 'confirmed'],
        ];
    }

    /**
     * Perform a real time validation
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    // /**
    //  * Edit profile method
    //  *
    //  * @param User $user
    //  * @return redirect
    //  */
    // public function editProfile(User $user)
    // {
    //     $attributes = $this->validate();

    //     if ($this->avatar) {
    //         $this->avatar = $this->avatar->store('avatars');
    //     }

    //     if ($this->banner) {
    //         $this->banner = $this->banner->store('banner');
    //     }

    //     $attributes = array_merge($attributes, [
    //         'avatar' => $this->avatar,
    //         'banner' => $this->banner,
    //     ]);

    //     $user->update($attributes);

    //     session()->flash('success', 'Profile data changed!');
    //     return redirect($user->path());
    // }

    /**
     * Return Edit profile page
     *
     * @return view
     */
    public function render()
    {
        return view('livewire.edit-profile-form');
    }
}
