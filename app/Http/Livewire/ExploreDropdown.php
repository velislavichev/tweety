<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;

class ExploreDropdown extends Component
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * @var array
     */
    public $searchResults = [];

    /**
     * Return search dropdown data
     *
     * @return view
     */

    public function render()
    {
        $this->searchResults = User::explore($this->search);

        return view('livewire.explore-dropdown', [
            'searchResults' => $this->searchResults
        ]);
    }
}
