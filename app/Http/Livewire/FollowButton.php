<?php

namespace App\Http\Livewire;

use App\Notifications\Followed;
use App\User;
use Livewire\Component;

class FollowButton extends Component
{
    /**
     * User model object
     *
     * @var object
     */
    public User $user;

    /**
     * Toggle follow method
     *
     * @return void
     */
    public function toggleFollow()
    {
        // sleep(1);wire:loading.class.remove="hidden"
        $currentUser = auth()->user();

        $currentUser->toggleFollow($this->user);

        if (!$currentUser->following($this->user)) {
            $this->user->notify(new Followed($currentUser->username, 'unfollow'));

            $this->emit('alert', ['type' => 'success', 'message' => __("You <b>unfollowed</b> {$this->user->username}!")]);
        } else {

            $this->user->notify(new Followed($currentUser->username, 'follow'));

            $this->emit('alert', ['type' => 'success', 'message' => __("You <b>followed</b> {$this->user->username}!")]);
        }

        // Refresh timeline data
        $this->emit('refreshFollowingList');
    }

    /**
     * Return livewire view component
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.follow-button');
    }
}
