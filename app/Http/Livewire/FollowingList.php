<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FollowingList extends Component
{
    /**
     * Listen for newly created tweet
     *
     * @var array
     */
    protected $listeners = ['refreshFollowingList' => 'render'];
 
    /**
     * Return livewire view component
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.following-list');
    }
}
