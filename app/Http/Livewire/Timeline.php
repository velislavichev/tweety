<?php

namespace App\Http\Livewire;

use App\Tweet;
use App\User;
use Livewire\Component;
use Livewire\WithPagination;

class Timeline extends Component
{
    use WithPagination;

    /**
     * @var string
     */
    public $profile;

    /**
     * @var object
     */
    public User $user;

    /**
     * Listen for newly created tweet
     *
     * @var array
     */
    protected $listeners = ['refreshTimeline' => 'render'];

    /**
     * Like method
     *
     * @param Tweet $tweet
     * @return void
     */
    public function like(Tweet $tweet)
    {
        $tweet->like();
    }

    /**
     * Dislike method
     *
     * @param Tweet $tweet
     * @return void
     */
    public function dislike(Tweet $tweet)
    {
        $tweet->dislike();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tweet $tweet
     * @return redirect
     */
    public function destroy(Tweet $tweet)
    {
        Tweet::destroy($tweet->id);
    }

    /**
     * Return livewire view component
     *
     * @return void
     */
    public function render()
    {
        // Check if timeline component is called in profile page or landing page.
        return view('livewire.timeline',  [
            'tweets' => $this->profile ? $this->user->profile() : auth()->user()->timeline()
        ]);
    }
}
