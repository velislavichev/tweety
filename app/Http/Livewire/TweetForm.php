<?php

namespace App\Http\Livewire;

use App\Tweet;
use Livewire\Component;
use Livewire\WithFileUploads;

class TweetForm extends Component
{
    use WithFileUploads;

    /**
     * 
     * @var string
     */
    public $body;

    /**
     * 
     * @var object
     */
    public $image;

    /**
     * 
     * @var string
     */
    public $success;

    /**
     * Store validation rules
     *
     * @var array
     */
    protected $rules = [
        'body' => 'required|max:255',
        'image' => 'nullable|file',
    ];

    /**
     * Store a newly created resource in storage.
     *
     * @return redirect
     */
    public function store()
    {
        $this->validate();

        if ($this->image) {
            $this->image = $this->image->store('images');
        }

        Tweet::create([
            'user_id' => auth()->id(),
            'body'    => $this->body,
            'image'   => $this->image ?? null,
        ]);

        // Reset Form data
        $this->reset();

        // Refresh timeline data
        $this->emit('refreshTimeline');

        $this->emit('alert', ['type' => 'success', 'message' => 'Yay! New tweet!']);
    }

    /**
     * Return livewire view component
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.tweet-form');
    }
}
