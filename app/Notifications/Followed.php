<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Followed extends Notification
{
    use Queueable;

    /**
     * 
     * @var string
     */
    public $username;

    /**
     * Follow / unfollow
     *
     * @var string
     */
    public $action;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($username, $action)
    {
        $this->username = $username;
        $this->action = $action;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'username' => $this->username,
            'action'   => $this->action
        ];
    }
}
