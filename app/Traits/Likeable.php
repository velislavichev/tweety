<?php

namespace App\Traits;

use App\Like;
use App\User;
use Illuminate\Database\Eloquent\Builder;

trait Likeable
{
    /**
     * Return all likes attached with the object
     *
     * @param Builder $query
     * @return object
     */
    public function scopeWithLikes(Builder $query)
    {
        return $query->leftJoinSub(
            'select tweet_id, sum(liked) as likes, sum(!liked) as dislikes from likes group by tweet_id',
            'likes',
            'likes.tweet_id',
            'tweets.id'
        );
    }

    /**
     * Like method
     *
     * @param object $user
     * @param boolean $liked
     * @return void
     */
    public function like($user = null, $liked = true)
    {
        $user ??= auth()->user();

        // Check if tweet has been liked or disliked and if so toggle it by removing from db.
        if ($this->isLikedBy($user) && $liked || $this->isDislikedBy($user) && !$liked) {
            $this->likes->where('user_id', $user->id)->first()->delete();
        } else {

            $this->likes()->updateOrCreate([
                'user_id' => $user->id
            ], [
                'liked' => $liked
            ]);
        }
    }

    /**
     * Dislike method
     *
     * @param object $user
     * @return void
     */
    public function dislike($user = null)
    {
        return $this->like($user, false);
    }

    /**
     * Check if tweet is liked
     *
     * @param User $user
     * @return boolean
     */
    public function isLikedBy(User $user)
    {
        return (bool) ($user->likes ? $user->likes->where('tweet_id', $this->id)->where('liked', true)->count() : 0);
    }

    /**
     * Check if tweet is disliked
     *
     * @param User $user
     * @return boolean
     */
    public function isDislikedBy(User $user)
    {
        return (bool) ($user->likes ? $user->likes->where('tweet_id', $this->id)->where('liked', false)->count() : 0);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
