<?php

namespace App;

use App\Traits\Likeable;
use Illuminate\Database\Eloquent\Model;


class Tweet extends Model
{
    use Likeable;

    public $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/' . $value) : null;
    }
}
