// Toastr library options
// window.toastr = require('toastr');

toastr.options = {
    "positionClass": "toast-bottom-right",
}

window.livewire.on('alert', param => {
    toastr[param['type']](param['message']);
});