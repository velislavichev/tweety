<x-master>
    <div class="container mx-auto flex justify-center py-4">
        <div class="px-6 py-4 bg-gray-400 rounded-lg border border-blue-400 w-1/4">
            <div class="col-md-8">
                <div class="card">
                    <div class="text-xl font-bold mb-4">{{ __('Register') }}</div>


                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="mb-6">
                                <label class="block mb-2 uppercase font-bold text-xs text-gray-700"
                                    for="email">{{ __('Username') }}</label>


                                <div class="col-md-6">
                                    <input id="username" type="text" class="border border-gray-400 p-2 w-full"
                                        name="username" value="{{ old('username') }}" required autocomplete="name"
                                        autofocus>

                                    @error('username')
                                    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-6">

                                <label for="name"
                                    class="block mb-2 uppercase font-bold text-xs text-gray-700">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="border border-gray-400 p-2 w-full " name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-6">
                                <label for="email"
                                    class="block mb-2 uppercase font-bold text-xs text-gray-700">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="border border-gray-400 p-2 w-full "
                                        name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>

                                    @enderror
                                </div>
                            </div>

                            <div class="mb-6">
                                <label for="password"
                                    class="block mb-2 uppercase font-bold text-xs text-gray-700">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="border border-gray-400 p-2 w-full "
                                        name="password" required autocomplete="new-password">

                                    @error('password')
                                    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>

                                    @enderror
                                </div>
                            </div>

                            <div class="mb-6">
                                <label for="password-confirm"
                                    class="block mb-2 uppercase font-bold text-xs text-gray-700">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password"
                                        class="border border-gray-400 p-2 w-full " name="password_confirmation" required
                                        autocomplete="new-password">
                                </div>
                            </div>

                            <div class="my-2">
                                <button type="submit"
                                    class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500">
                                    {{ __('Register') }}
                                </button>

                                <a class="btn btn-link pl-2 text-gray-800 hover:text-blue-500" href="{{ route('login') }}">
                                    {{ __('or Login') }}
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-master>
