<x-master>
    <section class="px-8">
        <main class="container mx-auto py-4">
            <div class="lg:flex lg:justify-between">
                <div class="lg:w-1/6">
                    @include('sidebar_links')
                </div>
            
                <div class="lg:flex-1 lg:mx-10">
                    
                    {{-- @yield('content') --}}
                    {{ $slot }}
            
                </div>
            
                <div class="lg:w-1/6 hidden lg:block">
                    <div class="bg-blue-100 rounded-lg mt-4 lg:mt-0 p-4 border">
                        @livewire('following-list')
                    </div>
                </div>
            </div>
            
        </main>
    </section>
</x-master>