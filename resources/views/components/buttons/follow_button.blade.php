@if (current_user()->isNot($user))

    <form method="POST" action=" {{ route('follow', $user->username) }}">
        @csrf

        <button type="submit" class="bg-blue-500 hover:bg-blue-600 rounded-full shadow py-4 px-4 text-white text-xs">
            {{ current_user()->following($user) ? 'Unfollow me' : 'Follow Me' }}
        </button>
    </form>

@endif
