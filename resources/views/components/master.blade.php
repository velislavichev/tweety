<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/png" href="/images/favicon.png" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <!-- Toastr library css -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />

    <!-- Livewire CSS -->
    @livewireStyles

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <!-- Toastr library js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    <!-- Alpine js -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.min.js" defer></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body class="flex flex-col h-screen justify-between">
    <div id="app">

        <section class="px-8 pt-4 mb-4 bg-blue-100 border-b">
            <header class="container mx-auto">

                <nav class="flex flex-col sm:flex-row space-y-4 sm:space-y-0 items-center justify-between flex-wrap pb-4">
                    <h1 class="">
                        <a href="/tweets" class="flex items-center">
                            <img src="/images/logo.svg" alt="Tweety">
                            <p class="font-bold text-xl -ml-18 hover:text-blue-500">
                                Tweety
                            </p>
                        </a>
                    </h1>

                    @auth
                        <!-- Explore search dropdown -->
                        @livewire('explore-dropdown')

                        <div class="flex items-center">

                            @livewire('notifications')

                            <!-- Profile avatar -->
                            <a href="{{ route('profile', auth()->user()->username) }}" class="font-semibold text-lg mr-4">
                                <img class="rounded-full" src="{{ auth()->user()->avatar }}" alt="" height="50" width="50">
                            </a>

                            <a href="#">

                                <form method="POST" action="/logout">
                                    @csrf

                                    <button class="font-semibold text-lg block hover:text-blue-500">
                                        Logout
                                    </button>
                                </form>
                            </a>
                        </div>
                    @endauth

                </nav>


            </header>
        </section>

        {{ $slot }}
    </div>

    <footer class="text-center p-4 bg-blue-100">

        <p class="text-semibold text-lg">
            Tweety&reg;. All rights reserved.
        </p>

    </footer>

    <!-- Livewire JS -->
    @livewireScripts

    <!-- Custom scripts -->
    @stack('scripts')
</body>

</html>
