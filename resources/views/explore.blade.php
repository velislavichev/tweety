<x-app>
    <div>
        @foreach ($users as $user)
            <a href="{{ $user->path() }}" class="flex items-center mb-5">
                <img src="{{ $user->avatar }}" alt="{{ $user->name }}`s avatar" width="40px;" class="rounded">

                <div>
                    <h4 class="font-bold pl-2">{{ '@' . $user->name }}</h4>
                </div>
            </a>
        @endforeach
    </div>
</x-app>