<div>
    {{-- <form wire:submit.prevent="editProfile({{ $user->id }})" action="{{ $user->path() }}" method="post" --}}
    <form action="{{ $user->path() }}" method="post"
        enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="username">Username</label>

            <input wire:model="username" class="border border-gray-400 p-2 w-full" type="text" name="username" id="username"
                value="{{ old('username') ?? $user->username }}" required>

            @error('username')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="name">Name</label>

            <input wire:model="name" class="border border-gray-400 p-2 w-full" type="text" name="name" id="name" value="{{ $user->name }}"
                required>

            @error('name')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="description">Description</label>

            <textarea wire:model="description" class="border border-gray-400 p-2 w-full" name="description"
                id="description">{{ old('description') ?? $user->description }}</textarea>

            @error('description')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <!-- Avatar -->
        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="avatar">Avatar</label>

            <div class="flex">

                <input wire:model="avatar" class="border border-gray-400 p-2 w-full" type="file" name="avatar" id="avatar"
                    value="{{ $user->avatar }}">

                <img src="{{ $user->avatar }}" alt="Your image" width="60px" class="rounded-full">
            </div>

            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <!-- Banner Image -->
        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="banner">banner</label>

            <div class="flex">

                <input wire:model="banner" class="border border-gray-400 p-2 w-full" type="file" name="banner" id="banner"
                    value="{{ $user->banner }}">

                <img src="{{ $user->banner }}" alt="Your image" width="60px" class="rounded-full">
            </div>

            @error('banner')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="email">Email</label>

            <input wire:model="email" class="border border-gray-400 p-2 w-full" type="email" name="email" id="email"
                value="{{ $user->email }}" required>

            @error('email')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password">Password</label>

            <input wire:model="password" class="border border-gray-400 p-2 w-full" type="password" name="password" id="password">

            @error('password')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password_confirmation">Confirm
                password</label>

            <input wire:model="password_confirmation" class="border border-gray-400 p-2 w-full" type="password" name="password_confirmation"
                id="password_confirmation">

            @error('password_confirmation')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <button type="submit"
                class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500 mr-4">Submit</button>

            <a href="{{ $user->path() }}" class="hover:underline">Cancel</a>
        </div>

    </form>
</div>
