<div>
    <div class="relative" x-data="{ isVisible: true }" @click.away="isVisible = false">
        <input wire:model="search" type="search"
            class="text-sm border-2 rounded-full focus:outline-none focus:shadow-outline w-48 sm:w-64  px-3 pl-8 py-1 text-gray-800"
            placeholder="Explore (Press '/' to focus)" x-ref="search" @keydown.window="
                if (event.code == 'Slash') {
                    event.preventDefault();
    
                    $refs.search.focus();
                }
            " @focus="isVisible = true" @keydown="isVisible = true" @keydown.escape.window="isVisible = false"
            @keydown.shift.tab="isVisible = false">

        <div class="absolute top-0 flex items-center h-full ml-2">
            <svg class="fill-current text-gray-400 w-4" viewBox="0 0 24 24">
                <path class="heroicon-ui"
                    d="M16.32 14.9l5.39 5.4a1 1 0 01-1.42 1.4l-5.38-5.38a8 8 0 111.41-1.41zM10 16a6 6 0 100-12 6 6 0 000 12z" />
            </svg>
        </div>

        <div wire:loading class="absolute top-0 right-0 mr-6 mt-1">
            <svg class="animate-spin mr-2 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 24 24">
                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                <path class="opacity-75" fill="currentColor"
                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z">
                </path>
            </svg>
        </div>

        @if (strlen($search) >= 2)

            <div class="absolute bg-blue-100 border-2 z-50 text-sm rounded w-48 sm:w-64 mt-2"
                x-show.transition.opacity.duration.300="isVisible">

                @if (count($searchResults))
                    <ul>

                        @foreach ($searchResults as $user)

                            <li class="hover:bg-blue-200 {{ !$loop->last ? 'border-b' : '' }}">

                                <a href="{{ $user->path() }}" class="flex items-center p-2">
                                    <img class="h-10 w-10 object-cover" src="{{ $user->avatar }}" alt="{{ $user->name }}`s avatar"
                                        class="rounded">

                                    <div>
                                        <h4 class="font-bold pl-2">{{ '@' . $user->name }}</h4>
                                    </div>
                                </a>

                            </li>

                        @endforeach
                    </ul>
                @else
                    <div class="p-3">No users found</div>
                @endif
                
            </div>

        @endif

    </div>

</div>
