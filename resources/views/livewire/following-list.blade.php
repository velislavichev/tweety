<div>
    <h3  class="font-bold text-xl mb-4">Following</h3>

    <ul>
        @forelse(auth()->user()->follows as $user)
            <li class="{{ $loop->last ? '' : 'mb-4' }}">
                <a href="{{ route('profile', $user->username) }}" class="flex items-center text-sm">

                    <img class="h-12 w-12 object-fit rounded-full mr-2" src=" {{ $user->avatar }}" alt="" width="40" height="40">

                    {{ $user->name }}
                </a>
            </li>
        @empty
            <p>No friends yet!</p>
        @endforelse
    </ul>

</div>
