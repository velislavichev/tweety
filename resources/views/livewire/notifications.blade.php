<div wire:poll>

    <a class="flex mr-4" href="{{ route('notifications') }}">

        @if (count(auth()->user()->unreadNotifications) && !request()->is('notifications'))

            <span class="flex h-3 w-3 -mr-10">
                <span class="relative animate-ping inline-flex rounded-full h-3 w-3 bg-red-500"></span>
            </span>

        @endif

        <div
            class="w-10 h-10 flex items-center justify-center transition ease-in-out duration-150 hover:bg-gray-300 rounded-full">

            <img class="h-6" src="/images/bell.svg" alt="Notifications">
        </div>
    </a>

</div>
