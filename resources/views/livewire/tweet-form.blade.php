<div>
    <!-- Success message -->
    @if ($message = session('success'))
        <x-messages.success :message="$message" />
    @endif


    <div class="border border-blue-400 rounded-lg px-8 py-6 mb-8">

        <form wire:submit.prevent="store" method="POST" action="/tweets" enctype="multipart/form-data">
            @csrf

            <div>
                <textarea wire:model.debounce.100ms="body" name="body" class="w-full"
                    placeholder="Whats up?">{{ old('body') }}</textarea>

                <p class="text-green-500 float-right py-3">
                    {{ strlen($body) }}
                </p>
            </div>

            @error('body')
            <p class=" text-red-400 text-sm mt-2"> {{ $message }}</p>
            @enderror

            <div class="mt-2 sm:mt-0 sm:col-span-2" x-data="{ isUploading: false, progress: 0 }"
                x-on:livewire-upload-start="isUploading = true" x-on:livewire-upload-finish="isUploading = false"
                x-on:livewire-upload-error="isUploading = false"
                x-on:livewire-upload-progress="progress = $event.detail.progress">

                <!-- Image -->
                <div class="my-3">

                    <input wire:model="image" class="border border-gray-400 p-2 w-full" type="file" name="image"
                        id="image">

                    @error('image')
                    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <!-- Progress Bar -->
                <div class="mt-4" x-show="isUploading">
                    <progress max="100" class=" bg-blue" x-bind:value="progress"></progress>
                </div>
            </div>


            @if ($image)
                <img src="{{ $image->temporaryUrl() }}">
            @endif

            <hr class="my-4">

            <footer class="flex justify-between items-center">

                <img class="h-12 w-12 object-fit rounded-full mr-2" src="{{ auth()->user()->avatar }}" alt="" height="50" width="50">

                <button type="submit"
                    class="bg-blue-500 hover:bg-blue-600 rounded-lg shadow py-2 px-2 text-white">Tweet!</button>
            </footer>

        </form>
    </div>

</div>
