<x-app>

    <h3 class="font-bold text-xl mb-4">
        New notifications
    </h3>

    <ul>

        @forelse ($unreadNotifications as $notification)
            <li>
                @if ($notification->data['action'] == 'follow')
                    <span class="font-bold text-blue-500"> Yay! New Follower! </span>
                @else
                    <span class="font-bold text-red-500"> Oh no! Lost Follower! </span>
                @endif

                You have been {{ $notification->data['action'] }} by
                <a href="{{ route('profile', $notification->data['username']) }}"
                    class="font-bold text-blue-600">{{ '@' . $notification->data['username'] }}</a> -
                {{ $notification->created_at->diffForHumans() }}
            </li>
        @empty
            <li>
                You`re up to date.
            </li>
        @endforelse

    </ul>

    <h3 class="font-bold text-xl my-4">
        Notifications
    </h3>

    <ul>

        @forelse ($readNotifications as $notification)
            <li>

                You have been {{ $notification->data['action'] }} by
                <a href="{{ route('profile', $notification->data['username']) }}"
                    class="font-bold text-blue-600">{{ '@' . $notification->data['username'] }}</a> -
                {{ $notification->created_at->diffForHumans() }}

            </li>
        @empty
            <li>
                You`re up to date.
            </li>
        @endforelse
    </ul>

</x-app>
