<x-app>
    @livewire('edit-profile-form', [
        'user' => $user
    ])
</x-app>
