<x-app>

    <!-- Success message -->
    @if ($message = session('success'))
        <x-messages.success :message="$message" />
    @endif

    <header class="mb-6 relative">

        <div class="relative">
            <img src="{{ $user->banner }}" alt="User banner" class="mb-2 object-center h-64 w-full object-cover">

            <img src="{{ $user->avatar }}" alt="User banner"
                class="h-32 w-32 object-cover rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2" style="left: 50%"
                width="150">
        </div>

        <div class="flex justify-between items-center">
            <div style="max-width:270px;">
                <h2 class="font-bold text-2xl mb-0">{{ $user->name }}</h2>
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
            </div>

            <div class="flex">
                @can('edit', $user)
                    <a href="{{ $user->path('edit') }}"
                        class="rounded-full border border-gray-400 hover:bg-gray-300 py-4 px-4 text-black text-xs mr-2">Edit
                        Profile</a>
                @endcan

                @livewire('follow-button', [
                'user' => $user
                ])

            </div>

        </div>


        <p class="text-sm my-10">
            @if ($description = $user->description)
                {{ $description }}
            @else
                Oh no :( <br> {{ $user->username == auth()->user()->username ? 'You' : $user->username }} didn`t
                describe
                {{ $user->username == auth()->user()->username ? 'yourself' : 'himself' }}.
            @endif
        </p>

    </header>

    <!-- Timeline section -->
    @livewire('timeline', [
    'profile' => true,
    'user' => $user
    ])

</x-app>
