<div class="mb-6 bg-blue-100 rounded-lg lg:mt-0 p-4 border">

    <ul class="flex flex-row justify-center space-x-12 lg:space-x-0 lg:flex-col">
        <li>
            <a href="{{ route('home') }}" class="font-bold text-lg lg:mb-4 block hover:text-blue-500">
                Home
            </a>
        </li>
        <li>
            <a href="/explore" class="font-bold text-lg block hover:text-blue-500">
                Explore
            </a>
        </li>
    </ul>
</div>
