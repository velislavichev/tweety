<x-app>

    <!-- Livewire approach -->
    @livewire('tweet-form')
    
    <!-- Timeline section -->
    @livewire('timeline')

    {{-- <!-- Success message -->
    <x-messages.success />

    @include('publish-tweet-panel')

    @include('timeline') --}}
</x-app>
