<div class=" p-4 {{ $loop->last ? '' : 'border-b border-b-gray-400' }}">
    <div class="flex">

        <div class="mr-2 flex-shrink-0 ">

            <a href="{{ route('profile', $tweet->user) }}">
                <img class="h-12 w-12 object-fit rounded-full mr-2" src="{{ $tweet->user->avatar }}" alt="" width="50" height="50">
            </a>

        </div>

        <div class="w-full">
            <div class="flex justify-between items-center mb-4">
                <h5 class="font-bold">

                    <a href="{{ route('profile', $tweet->user) }}">
                        {{ $tweet->user->name }}
                    </a>
                </h5>

                @can('delete', $tweet)

                    <a wire:click="destroy({{ $tweet->id }})" class="cursor-pointer">
                        <p class="bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded-full text-sm">Remove
                        </p>
                    </a>

                @endcan
            </div>

            <p class="text-sm mb-3">
                {{ $tweet->body }}
            </p>

            @if ($image = $tweet->image)
                <p class="mb-3">
                    <img src="{{ $image }}" alt="Tweet image">
                </p>
            @endif

        </div>
    </div>

    <div class="flex ml-16">

        <!-- Likes  -->
        <x-buttons.likes_buttons :tweet="$tweet" />

    </div>

</div>
