<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main page
Route::get('/', 'Auth\LoginController@showLoginForm');

// Auth controllers
Auth::routes();

Route::middleware('auth')->group(function () {
    // Tweets controller
    Route::get('/tweets', 'TweetsController@index')->name('home');
    Route::post('/tweets', 'TweetsController@store');
    Route::delete('/tweets/{tweet}', 'TweetsController@destroy')->middleware('can:delete,tweet');

    Route::post('/tweets/{tweet}/like', 'TweetsController@like')->name('like');
    Route::delete('/tweets/{tweet}/dislike', 'TweetsController@dislike')->name('dislike');

    // User Profile controller
    Route::post('/profiles/{user:username}/follow', 'FollowersController@store')->name('follow');
    Route::get('/profiles/{user:username}/edit', 'ProfilesController@edit')->middleware('can:edit,user');
    Route::patch('/profiles/{user:username}', 'ProfilesController@update')->middleware('can:edit,user');
    
    Route::get('/profiles/{user:username}', 'ProfilesController@show')->name('profile');
    
    // User Notifications controller
    Route::get('/notifications', 'NotificationsController')->name('notifications');

    // Explore controller
    Route::get('/explore', 'ExploreController');
});

