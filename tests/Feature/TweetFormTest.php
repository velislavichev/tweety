<?php

namespace Tests\Feature;

use App\Http\Livewire\TweetForm;
use App\Tweet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Livewire\Livewire;

class TweetFormTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        // Login user
        $user = factory(\App\User::class)->create();
        $this->be($user);
    }

    /** @test */
    public function can_create_tweet()
    {
        Livewire::test(TweetForm::class)
            ->set('body', 'Test body')
            ->call('store')
            ->assertStatus(200);

        $this->assertTrue(Tweet::whereBody('Test body')->exists());
    }

    /** @test  */
    function can_set_initial_body()
    {
        Livewire::test(TweetForm::class, ['body' => 'Test body'])
            ->assertSet('body', 'Test body');
    }

    /** @test  */
    function body_is_required()
    {
        Livewire::test(TweetForm::class)
            ->set('body', '')
            ->call('store')
            ->assertHasErrors(['body' => 'required']);
    }

    /** @test */
    public function can_upload_photo()
    {
        $this->markTestSkipped('Must be revisited.');

        Storage::fake('avatars');

        $file = UploadedFile::fake()->image('avatar.png');

        Livewire::test(TweetForm::class)
            ->set('image', $file)
            ->call('store', 'uploaded-avatar.png');

        Storage::disk('avatars')->assertExists('uploaded-avatar.png');
    }
}
